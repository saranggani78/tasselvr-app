import 'dart:async';
import 'dart:convert' as convert;
import 'dart:io';
//https://pub.dartlang.org/packages/flutter_advanced_networkimage
import 'package:http/http.dart' as http;
import 'package:parse_fullstack/models/user_data.dart';
import 'package:dio/dio.dart';
import 'package:parse_fullstack/models/city.dart';

// ignore_for_file: non_constant_identifier_names

// https://github.com/lotux/parse_server_dart
  // rest api key (master key)  : 3d4fa4aba913f09dae22f15cf27a198e26e325c9
  // file key : 88e4f7776f786e0d00c4f3f5001a1b6c6bafd618

// https://pub.dartlang.org/documentation/http/latest/
// https://pub.dartlang.org/documentation/http/latest/http/Request-class.html
// https://pub.dartlang.org/packages/http
// using parse server rest api
// implement services for CRUD
// profile
// trip
// chatroom
// offers (in app messages using parse push notification)
// http interface
// https://v1-dartlang-org.firebaseapp.com/dart-vm/dart-by-example#making-a-post-request
class ParseService {

  String PARSE_APP_ID = "yi5xfRZojERouKNKblwVqiTtEqUSSpdiJAAqBmYb";
  String REST_API_KEY = "HHbC9ZNDAsl7uwJH4RvH4tEPkJOHkDCmfns6oFV7";
  String PARSE_URI = "https://parseapi.back4app.com/";

  Future<List<City>> city_list;
  Future<List<UserData>> user_list;

  ParseService() {
    city_list = getCityList();
    user_list = buildUserList();
  }

  static ParseService _instance;
  static ParseService getInstance() {
    if (_instance == null) {
      _instance = ParseService();
    }
    return _instance;
  }

  // json formatted
  //   {
  //   "text":"test test",
  //   "email":"ctalladen78@gmail.com",
  //   "likes":1,
  //   "tags":[1,"lkljk", "3333"],
  //   "publishDate":{ "__type": "Date", "iso": "2018-11-06T18:02:52.249Z" },
  //   "owner":{ "__type": "Pointer", "className": "_User", "objectId": "<THE_REFERENCED_OBJECT_ID>" },
  //   "showComments":true
  //   }
  Future<List<UserData>> getReviewsList({userId: String}) async {
  
  }

  Future<bool> createNewReview({description : String, date: String, duration: int, city: String, tripId: String, }) async {
    // trip.isReviewed == false
    // var header = {
    //   "X-Parse-Application-Id":PARSE_APP_ID,
    //   "X-Parse-REST-API-Key":REST_API_KEY,
    // };
    // var options = Options(headers: header);
    // var data = {};
    // String uri = PARSE_URI+"classes/Trip";
    // var response = await Dio().post(uri, data: data,options: options).catchError((onError){print("USER error $onError");});
    // return (response.statusCode == 201) ?  true : false;
  }

  Future<List<UserData>> getOffersList({userId: String}) async {
  
  }

  Future<bool> createNewOffer({description : String, date: String, duration: int, city: String, tripId: String, }) async {
    // trip.isReviewed == false
    // var header = {
    //   "X-Parse-Application-Id":PARSE_APP_ID,
    //   "X-Parse-REST-API-Key":REST_API_KEY,
    // };
    // var options = Options(headers: header);
    // var data = {};
    // String uri = PARSE_URI+"classes/Offer";
    // var response = await Dio().post(uri, data: data,options: options).catchError((onError){print("USER error $onError");});
    // return (response.statusCode == 201) ?  true : false;
  }    

  Future<List<String>> getConnectionsList({userId: String}) async {
    var header = {
      "X-Parse-Application-Id":PARSE_APP_ID,
      "X-Parse-REST-API-Key":REST_API_KEY,
    };
    var options = Options(headers: header);
    var data = {};
    String uri = PARSE_URI+"classes/_User";
    var response = await Dio().get(uri, data: data, options: options).catchError((onError){print("USER error $onError");});
    // TODO get users.connection list
    //  for(var user in response.data["results"]["connectionList"])
    // var res = response.data["results"];
    // var conn = res['connectionList'];

    // print("GET CONNECTIONS ${res}");
    List<String> userList = []; 
    if(response.statusCode == HttpStatus.ok){
      for(var user in response.data["results"]){
        // print(" DATA $user");
        // var uo = UserData.fromJson(id);
        for(var connection in user["connectionList"]){
          userList.add(connection);
        }
        // userList.add(id);
      }
    }
    return userList;
  }

  Future<List<UserData>> getTripsList({userId: String}) async {
    List<UserData> list = [];
    UserData user = UserData();
    var header = {
      "X-Parse-Application-Id":PARSE_APP_ID,
      "X-Parse-REST-API-Key":REST_API_KEY,
    };
    var options = Options(headers: header);
    var data = {};
    String uri = PARSE_URI+"classes/Trip";
    var response = await Dio().get(uri, data: data, options: options).catchError((onError){print("USER error $onError");});
    List<UserData> userList = []; 
    if(response.statusCode == HttpStatus.ok){
      for(var u in response.data["results"]){
        // print("USER DATA $u");
        var uo = UserData.fromJson(u);
        userList.add(uo);
      }
    }
    return userList;
  }

  Future<bool> createNewTrip({description : String, date: String, duration: int, city: String, tripId: String, }) async {
    // trip.isReviewed == false
    var header = {
      "X-Parse-Application-Id":PARSE_APP_ID,
      "X-Parse-REST-API-Key":REST_API_KEY,
    };
    var options = Options(headers: header);
    var data = {};
    String uri = PARSE_URI+"classes/Trip";
    var response = await Dio().post(uri, data: data,options: options).catchError((onError){print("USER error $onError");});
    return (response.statusCode == 201) ?  true : false;
  }

  // https://dashboard.back4app.com/apidocs#Trip-updating-objects
  // Any keys you don’t specify will remain unchanged
  Future<bool> updateTrip({description : String, date: String, duration: int, city: String, tripId: String, }) async {
    // trip.isReviewed == false
    var header = {
      "X-Parse-Application-Id":PARSE_APP_ID,
      "X-Parse-REST-API-Key":REST_API_KEY,
    };
    var options = Options(headers: header);
    var data = {}; // Trip.toJson()
    String uri = PARSE_URI+"classes/Trip/"+tripId;
    var response = await Dio().post(uri, data: data,options: options).catchError((onError){print("USER error $onError");});
    return (response.statusCode == 200) ?  true : false;
  }

  Future<List<UserData>> getUserList() async {
    var header = {
      "X-Parse-Application-Id":PARSE_APP_ID,
      "X-Parse-REST-API-Key":REST_API_KEY,
    };
    var options = Options(headers: header);
    String uri = PARSE_URI+"classes/_User";
    var response = await Dio().get(uri, options: options).catchError((onError){print("USER error $onError");});
    List<UserData> userList = []; 
    if(response.statusCode == HttpStatus.ok){
      for(var u in response.data["results"]){
        // print("USER DATA $u");
        var uo = UserData.fromJson(u);
        userList.add(uo);
      }
    }
    return userList;
  }

  Future<List<UserData>> buildUserList() async {
    List<UserData> userList = await getUserList();
    for(var user in userList){
      Map<String,String> city = {};
      city = await getCityImage(user.location).catchError((onError){print("GET CITY IMAGE ERROR $onError");});
      user.locationImage = city["locationImage"];
      user.city = city["city"];
    }
    return userList;
  }

  Future<Map<String,String>> getCityImage(String objectId) async{
    var header = {
      "X-Parse-Application-Id":PARSE_APP_ID,
      "X-Parse-REST-API-Key":REST_API_KEY,
    };
    var options = Options(headers: header);
    String uri = PARSE_URI + "classes/City/" + objectId;
    var response = await Dio().get(uri, options: options).catchError((onError){print("CITY error $onError");});
    // print("444 GET CITY city: ${response.data['city']} URL ${response.data['image']}");
    Map<String,String> o = {};
    o["locationImage"] = response.data["image"];
    o["city"] = response.data["city"];
    return o;
  }

  Future<List<City>> getCityList() async {
    var header = {
      "X-Parse-Application-Id":PARSE_APP_ID,
      "X-Parse-REST-API-Key":REST_API_KEY,
    };
    var options = Options(headers: header);
    String uri = PARSE_URI+"classes/City";
    var response = await Dio().get(uri, options: options).catchError((onError){print("CITY error $onError");});
    List<City> cityList = [];
    if(response.statusCode == HttpStatus.ok){
      for(var c in response.data["results"]){
        var cc = City.fromJson(c);
        cityList.add(cc);
      }
    }
    return cityList;
  } 

  // https://dashboard.back4app.com/apidocs#counting-objects
  Future<bool> countUsers() async {
    var header = {
      "X-Parse-Application-Id":PARSE_APP_ID,
      "X-Parse-REST-API-Key":REST_API_KEY,
    };
    var options = Options(headers: header);
    String uri = PARSE_URI+"requestPasswordReset";
    var queryParams = {"count": 1};
    return Dio().get(uri, data: queryParams, options: options)
    .then((response){
      print("COUNT users status: ${response.statusCode}");
      print("COUNT users data: ${response.data}");
      return true;
    })
    .catchError((error){
      return false;
    });
  }


  
  Future<bool> resetPassword({email:String}) async {
    var header = {
      "X-Parse-Application-Id":PARSE_APP_ID,
      "X-Parse-REST-API-Key":REST_API_KEY,
    };
    var options = Options(headers: header);
    String uri = PARSE_URI+"requestPasswordReset";
    var data = {"email": email};
    return Dio().post(uri, data: data, options: options)
    .then((response){
      print("RESET password result: ${response.statusCode}");
      return true;
    })
    .catchError((error){
      return false;
    });
  }

  //https://docs.parseplatform.org/rest/guide/#validating-session-tokens--retrieving-current-user 
  Future<UserData> checkCurrentSession(String token) async {

    // after log in the preferences object should have session token use this to get current session
    var header = {
      "X-Parse-Application-Id":PARSE_APP_ID,
      "X-Parse-REST-API-Key":REST_API_KEY,
      "X-Parse-Session-Token":token,
    };
    var options = Options(headers: header);
    String uri = PARSE_URI+"users/me";
    Response response = await Dio().get(uri, options: options);

    UserData user;
    print("CURRENT Session: ${response.data}");

    if (response.statusCode != 503){
      user = UserData.fromJson(response.data);  
    }
    return user;
  }

  void logout(String token) async {

    // after log in the preferences object should have session token use this to get current session
    var header = {
      "X-Parse-Application-Id":PARSE_APP_ID,
      "X-Parse-REST-API-Key":REST_API_KEY,
      "X-Parse-Session-Token":token,
    };
    var options = Options(headers: header);
    String uri = PARSE_URI + "logout";
    Response response = await Dio().post(uri, options: options);

    print("LOGOUT result : ${response.data}");
  }

  // TODO setup facebook accountkit phone validation there is a web api 
  Future<bool> phoneValidate({number: String}) async {
    return false;
  }

  Future<bool> emailValidate({username: String, password: String, email: String}) async {

    var header = {
      "X-Parse-Application-Id":PARSE_APP_ID,
      "X-Parse-REST-API-Key":REST_API_KEY,
    };
    var options = Options(headers: header);
    String uri = PARSE_URI + "verificationEmailRequest";
    var data = {"email": email};
    return Dio().post(uri, data: data, options: options).then((response){

      if(response.statusCode == 200){
        return true;
      }
    })
    .catchError((error){return false;});
  }

  // TODO flutter_facebook_plugin 
  Future<UserData> signInFacebook() async {
    
    return UserData();
  }

  // https://github.com/flutterchina/dio#request-options
  Future<UserData> signInEmail({password: String, username: String}) async {

    // save current token session in preferences object
    Map<String, String> queryParameters = {
      "username": username,
      "password": password,
    };

    var headers ={
        "X-Parse-Revocable-Session" : "1",
        "X-Parse-Application-Id":PARSE_APP_ID,
        "X-Parse-REST-API-Key":REST_API_KEY
      };
    var options = Options(headers: headers);
    String uri = PARSE_URI + "login";

    Response response = await Dio().get(uri, data:queryParameters, options: options);
    UserData user;

    print(response.headers);
    print("SIGN IN result:");
    print(response.data);

    if (response.statusCode != 503){
      user = UserData.fromJson(response.data);                                                   
    }
    return user;
  }

  // https://docs.parseplatform.org/rest/guide/#users-api
  Future<UserData> signUp({username: String, password: String, email: String}) async {
    UserData user;
    var client = http.Client();

    var headers ={
        "X-Parse-Application-Id":PARSE_APP_ID,
        "X-Parse-REST-API-Key":REST_API_KEY
      };
    String uri = PARSE_URI + "classes/_User";
    var response = await client.post(
      uri,
      headers: headers,
      body: {"email": email, "username": username, "password": password}
    );
    client.close();
    print(response.headers);
    print("SIGN UP result: ");
    print(response.body); 
    print(response.statusCode);

    if (response.statusCode != 503){
      print("RESPONSE OK");
      var jsonResponse = convert.jsonDecode(response.body); //https://www.dartlang.org/guides/libraries/library-tour#dartconvert---decoding-and-encoding-json-utf-8-and-more
      print(jsonResponse);
      user = UserData.fromJson(jsonResponse);                                                   
      print(user);
    }
    // only return null if server error
    return user;
  }

  Future<String> getCurrentUserId() async {
    var client = http.Client();
    var headers ={
        "X-Parse-Application-Id":PARSE_APP_ID,
        "X-Parse-REST-API-Key":REST_API_KEY
      };
    String uri = PARSE_URI + "sessions/me";
    var response = await client.get(
      uri,
      headers: headers
    );
    return '';
  }

}






