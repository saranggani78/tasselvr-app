import 'package:flutter/material.dart';

class Fluttertoast {
  static void showToast(context, {msg}) {
    Scaffold.of(context).showSnackBar(SnackBar(content: Text(msg)));
  }
}
