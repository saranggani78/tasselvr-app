// https://github.com/rohan20/flutter-chat-app/blob/master/lib/ChatScreen.dart
// https://github.com/ctalladen78/flutter-chat-demo/blob/master/lib/chat.dart

import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'tassel_colors.dart';

var currentUserEmail;
var _scaffoldContext;

class ChatThread extends StatefulWidget {
  final String chatId;

  ChatThread({Key key, this.chatId}) : super(key: key);

  @override
  ChatScreenState createState() {
    return new ChatScreenState();
  }
}

class ChatScreenState extends State<ChatThread> {
  final TextEditingController _textEditingController =
      new TextEditingController();
  bool _isComposingMessage = false;
  // final reference = FirebaseDatabase.instance.reference().child('messages');

  CupertinoButton getIOSSendButton() {
    return new CupertinoButton(
      child: new Text("Send"),
      onPressed: _isComposingMessage
          ? () => _textMessageSubmitted(_textEditingController.text)
          : null,
    );
  }

  IconButton getDefaultSendButton() {
    return new IconButton(
      icon: new Icon(Icons.send),
      onPressed: _isComposingMessage
          ? () => _textMessageSubmitted(_textEditingController.text)
          : null,
    );
  }

  Widget _buildTextComposer() {
    return new IconTheme(
        data: new IconThemeData(
          color: _isComposingMessage
              ? Theme.of(context).accentColor
              : Theme.of(context).disabledColor,
        ),
        child: new Container(
          margin: const EdgeInsets.symmetric(horizontal: 8.0),
          child: new Row(
            children: <Widget>[
              new Container(
                margin: new EdgeInsets.symmetric(horizontal: 4.0),
                child: new IconButton(
                    icon: new Icon(
                      Icons.photo_camera,
                      color: Theme.of(context).accentColor,
                    ),
                    onPressed: () async {
                      await _ensureLoggedIn();
                      //File imageFile = await ImagePicker.pickImage(source: ImageSource.camera);
                      int timestamp = new DateTime.now().millisecondsSinceEpoch;
                      // upload camera on background
                      // StorageReference storageReference = FirebaseStorage
                      //     .instance
                      //     .ref()
                      //     .child("img_" + timestamp.toString() + ".jpg");
                      // StorageUploadTask uploadTask =
                      //     storageReference.put(imageFile);
                      // Uri downloadUrl = (await uploadTask.future).downloadUrl;
                      // _sendMessage(
                      //     messageText: null, imageUrl: downloadUrl.toString());
                    }),
              ),
              new Flexible(
                child: new TextField(
                  controller: _textEditingController,
                  onChanged: (String messageText) {
                    setState(() {
                      _isComposingMessage = messageText.length > 0;
                    });
                  },
                  onSubmitted: _textMessageSubmitted,
                  decoration:
                      new InputDecoration.collapsed(hintText: "Send a message"),
                ),
              ),
              new Container(
                margin: const EdgeInsets.symmetric(horizontal: 4.0),
                child: Theme.of(context).platform == TargetPlatform.iOS
                    ? getIOSSendButton()
                    : getDefaultSendButton(),
              ),
            ],
          ),
        ));
  }

  Future<Null> _textMessageSubmitted(String text) async {
    _textEditingController.clear();

    setState(() {
      _isComposingMessage = false;
    });

    await _ensureLoggedIn();
    _sendMessage(messageText: text, imageUrl: null);
  }

  // push message to realtime db which publishes it to subscribers
  void _sendMessage({String messageText, String imageUrl}) {
    // reference.push().set({
    //   'text': messageText,
    //   'email': googleSignIn.currentUser.email,
    //   'imageUrl': imageUrl,
    //   'senderName': googleSignIn.currentUser.displayName,
    //   'senderPhotoUrl': googleSignIn.currentUser.photoUrl,
    // });
    // analytics.logEvent(name: 'send_message');
  }

  Future<Null> _ensureLoggedIn() async {

  }

  Future _signOut() async {
    Scaffold
        .of(_scaffoldContext)
        .showSnackBar(new SnackBar(content: new Text('User logged out')));
  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: TasselColors.dark_blue,
      appBar: new AppBar(
        backgroundColor: TasselColors.dark_green,
          title: new Text("Flutter Chat App"),
          actions: <Widget>[
            new IconButton(
                icon: new Icon(Icons.exit_to_app), onPressed: _signOut)
          ],
        ),
        body: new Container(
          child: new Column(
            children: <Widget>[
              new Flexible(
                child: StreamBuilder(
                  builder: (BuildContext context, AsyncSnapshot snapshot){
                    return Container(
                      child: Container(),
                    );
                  },
                ),
              ),
              // new Divider(height: 1.0),
              new Container(
                decoration:
                    new BoxDecoration(color: Theme.of(context).cardColor),
                child: _buildTextComposer(),
              ),
              // new Builder(builder: (BuildContext context) {
              //   _scaffoldContext = context;
              //   return new Container(width: 0.0, height: 0.0);
              // })
            ],
          ),
          ),
      //   persistentFooterButtons: <Widget>[
      //   Row(
      //     mainAxisSize: MainAxisSize.max,
      //     mainAxisAlignment: MainAxisAlignment.start,
      //     children: <Widget>[
      //       // https://stackoverflow.com/questions/53586892/flutter-textformfield-hidden-by-keyboard
      //       SizedBox(
      //         width: MediaQuery.of(context).size.width - 70.0,
      //         child: TextFormField(
      //           controller: _textEditingController,
      //           decoration: InputDecoration(
      //             hintText: "Type here...",
      //             contentPadding: EdgeInsets.symmetric(horizontal: 20.0),
      //             border: OutlineInputBorder(
      //               borderRadius: BorderRadius.circular(32.0)
      //             ),
      //           ),
      //         )
      //       ),
      //       IconButton(
      //         icon: Icon(Icons.send),
      //         onPressed: (){
      //           if(_textEditingController.text.isNotEmpty){
      //             // parseSDKService.sendMessage({"message": _nameController.text.toString()});
      //             print(_textEditingController.text);
      //           }
      //         },
      //       ),
      //       // SizedBox(width: 100.0,)
      //     ],
      //   ),
      // ]
    );
  }

}