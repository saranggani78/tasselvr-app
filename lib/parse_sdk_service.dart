import 'package:parse_server/parse_server.dart';

// https://github.com/lotux/parse_server_dart
// https://github.com/tensor-programming/flutter_websockets/blob/master/lib/main.dart
// https://flutter.io/docs/cookbook/networking/web-sockets
class ParseSDKService {

  ParseSDKService();

  // headers:{
  //   "X-Parse-Application-Id": ApplicationConstants.keyParseApplicationId,
  //   "X-Parse-Javascript-Key": ApplicationConstants.keyParseJavascript,
  //   "X-Parse-Master-Key": ApplicationConstants.keyParseMaster
  // } 

  void initParse(){
    var credentials = Credentials(ApplicationConstants.keyParseApplicationId, ApplicationConstants.keyParseMaster);
    var parse = Parse(credentials, ApplicationConstants.keyParseServerUrl, ApplicationConstants.keyParseLiveQueryChannel);
    LiveQuery liveQuery = parse.liveQuery();
    liveQuery.client;
    liveQuery.on("change", (change){print(change);});
  }

  void openSocketChannel() {
    print("SOCKEt init ");
    var connectMsg =
      {
        "op": "connect",
        // "restAPIKey": "",  // Optional 
        "javascriptKey": ApplicationConstants.keyParseJavascript, // Optional
        // "clientKey": "", //Optional
        // "windowsKey": "", //Optional
        "masterKey": ApplicationConstants.keyParseMaster, // Optional
        // "sessionToken": "" // Optional
      };

  }

  void onChangeHandler( var message) {
      print("LIVE QUERY HAndler $message");
  } 

  void sendMessage(Map<String,String> message) {
    print("Sending message $message");
  }

  void closeSocketChannel() {
  }

}

abstract class ApplicationConstants {
  static const String keyParseJavascript = "qpGqmR37ENscXPVVuFiGYhMsrVh52z6STzcDrfxz";
  static const String keyAppName = "chat-app-prototype";
  static const String keyParseApplicationId = "yi5xfRZojERouKNKblwVqiTtEqUSSpdiJAAqBmYb";
  static const String keyParseMaster = "mf2sNe3rdgiIPttJzIQ1A1tbrWlhTPxLR8q8PGTE";
  static const String keyParseServerUrl = "https://parseapi.back4app.com";
  static const String keyRestAPI = "HHbC9ZNDAsl7uwJH4RvH4tEPkJOHkDCmfns6oFV7";
  static const String keyParseLiveQueryChannel = "wss://chat-app-prototype.back4app.io";
}
