import 'package:flutter/material.dart';
import 'package:parse_fullstack/parse_rest_service.dart';
import 'tassel_colors.dart';

class CreateReviewForm extends StatefulWidget {
  _PageState createState() => _PageState();
}

// TODO on submit form create a new trip object
class _PageState extends State<CreateReviewForm> {
  TextEditingController reviewCtrl = TextEditingController();
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();


  void _cancelTrip() {
    // TODO Navigator.pop  // back to home

  }

  void submitForm() {

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: TasselColors.dark_blue,
        appBar: new AppBar(
            backgroundColor: TasselColors.dark_green,
        title: Text("New Trip"),
        actions: <Widget>[
          GestureDetector(
            child: Text("Cancel"),
            onTap: _cancelTrip,
          )
        ]
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: new Column(
            children: <Widget>[
              SizedBox(height: 20.0,),
              Text("Sign In",
                style: TextStyle(fontSize: 30.0),
              ),
              Stack(
                  alignment: const Alignment(1.0, 1.0),
                  children: <Widget>[
                    TextFormField(
                      controller: reviewCtrl,
                      decoration: InputDecoration(
                        labelText: "Username",
                      ),
                      // onSaved: (field) => data.addAll({"fieldkey": field}),
                      keyboardType: TextInputType.text,
                    ),
                    IconButton(icon: Icon(Icons.cancel), onPressed: (){reviewCtrl.clear();}),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        RaisedButton(
                          onPressed: (){submitForm();},
                          // child: Text("Sign in", style: TextStyle(fontSize: 16.0),),
                          child: Icon(Icons.send),
                          padding: EdgeInsets.fromLTRB(30.0, 15.0, 30.0, 15.0),
                          color: Color(0xffdd4b39),
                        )
                      ],
                    ),
                  ]
                ),
            ]
                
        ),
      ),
    )
    );
  }
}