import 'package:shared_preferences/shared_preferences.dart';
import 'package:parse_fullstack/parse_rest_service.dart';
import 'package:parse_fullstack/models/user_data.dart';
import 'dart:async';
// https://stackoverflow.com/questions/40404567/how-to-send-verification-email-with-firebase

// viewcontroller for login screen
class LoginService {

  ParseService parseService = ParseService.getInstance();
  // SharedPreferences prefs;
  UserData _currentUser = UserData();

  LoginService( );

  // if current user is null
  Future<bool> signUp({username: String, email: String , pwd: String}) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    UserData user = await parseService.signUp(username: username, email: email, password: pwd);
    bool validEmail = await parseService.emailValidate(username: username, email: email, password: pwd);
    print("Valid email: $validEmail");
    if(user != null && validEmail){
      print("USER signup : $user");
      await prefs.setString('objectId', user.objectId);
      await prefs.setString('username', user.username);
      await prefs.setString('email', user.email);
      await prefs.setString('sessionToken', user.sessionToken);
      return true;
    }
    return false;
  }

  Future<bool> emailValidate({email: String}) async {
    return await parseService.emailValidate(email:email);
  }

  Future<bool> resetPassword({email:String}) async {
    return await parseService.resetPassword(email: email);
  }

  // user existas but not currently logged in
  Future<bool> handleSignIn({username:String, password:String}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    UserData user = await parseService.signInEmail(username: username, password: password);

    if (user != null){
    // Write data to local
    // UserData user = await parseService.getCurrentUserData();
    await prefs.setString('objectId', user.objectId);
    await prefs.setString('username', user.username);
    await prefs.setString('email', user.email);
    await prefs.setString('sessionToken', user.sessionToken);
    return true;
    }
    return false;
  }

  Future<bool> checkCurrentSession() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String sessionTok = prefs.getString('sessionToken');
    print("SESSION $sessionTok");
    UserData user = await parseService.checkCurrentSession(sessionTok);
    if(user != null){
      return true;
    }
    return false;
  }

  Future<void> logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String sessionTok = prefs.getString('sessionToken');
    await parseService.logout(sessionTok);
    await prefs.setString('sessionToken', '');

  }

  // build a UserDAta from instagram login {objectId, username, email, sessionToken}
  Future<bool> facebookLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // get objectId
    // get username
    // get email
    // get session_token
    // UserData user = parseService.getUser(objectId)
    // String password = generateToken(); // use this to save in db, user can edit it using app ( edit profile screen)
    // TODO access_token = instagramLogin();
    // await parseService.updateUser(); 

  }

}