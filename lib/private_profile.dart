import 'package:flutter/material.dart';
import 'api_service.dart';
import 'toast_is_snackbar.dart';
import 'package:parse_fullstack/login_service.dart';
import 'package:parse_fullstack/routes.dart';
import 'tassel_colors.dart';

class PrivateProfileScreen extends StatefulWidget {

  final bool isPrivate;
  final String userId;
  PrivateProfileScreen({Key key, this.userId, this.isPrivate}) : super(key: key);

  @override
  _SliverSamplePageState createState() => new _SliverSamplePageState();
}

// https://medium.com/@diegoveloper/flutter-collapsing-toolbar-sliver-app-bar-14b858e87abe
class _SliverSamplePageState extends State<PrivateProfileScreen> {
  ApiService apiService = new ApiService();
  LoginService loginService = LoginService();

  void _logout() async {
    WidgetBuilder homePage = Routes(context).routes["/"];
    await loginService.logout(); 
    // Navigator.popUntil(context, ModalRoute.withName("/"));
    Navigator.pushNamedAndRemoveUntil(context, "/", (Route<dynamic> route) => false);
  }

  Widget _buildButtonRow() {
    return Container(
      // height: 20.0, 
      // padding: EdgeInsets.all(10.0),
      child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        _buildConnectButton(),
        SizedBox(width: 10.0),
        _buildMessageButton(),
      ],),
    );
  }

  Widget _buildConnectButton() {
    return InputChip(
      backgroundColor: Colors.greenAccent[200],
      avatar: CircleAvatar(
        radius: 50.0, 
        backgroundColor: Colors.white70,
        child: new Icon(Icons.add, size: 20.0, color: Colors.black87)
      ),
      label: new Text("Connections"),
      onPressed: (){
        Fluttertoast.showToast(context, msg: 'Added to connections');
      },
    );
  }
  Widget _buildMessageButton() {
      return InputChip(
        backgroundColor: Colors.orangeAccent[200],
        avatar: CircleAvatar(
                radius: 50.0, 
                backgroundColor: Colors.white70,
                child: new Icon(Icons.send, size: 15.0, color: Colors.black87,)
              ),            // avatar: new Icon(Icons.send, size: 20.0,),
        label: new Text("Message"),
        labelStyle: TextStyle(fontSize: 12.0, color: Colors.black87),
        // padding: EdgeInsets.all(10.0),
        onPressed: (){
              // TODO Navigate to chat screen with the user model sent as argument
        },
      );  
  }

  Widget _buildMapSection() {
    // map section
    return Padding(
      padding: EdgeInsets.all(10.0),
      child: Container(
        height: 200,
        width: MediaQuery.of(context).size.width,
        child: apiService.buildStaticMap(latitude: "testLat", longitude: "testLong", width: 300, height: 200),
      ),
    );
  }

  // https://kodestat.gitbook.io/flutter/29-flutter-drawer-which-shows-toggle-menu
  // https://gist.github.com/branflake2267/87e0a21eff8736a83646ef8c67c1ba6c
  Widget buildDrawer(BuildContext context) {
    return Drawer(
      child:  ListView(
      // Important: Remove any padding from the ListView.
      padding: EdgeInsets.zero,
      children: <Widget>[
          DrawerHeader(
            // child: Text('Drawer Header'),
            child: Row(children: <Widget>[
              Text("Drawer header"),
              IconButton(
                icon: Icon(Icons.cancel),
                onPressed: (){
                  Navigator.pop(context);
                },
              )
            ],),
            decoration: BoxDecoration(
              color: Colors.blue,
            ),
          ),
          ListTile(
            title: Text('Find Work', style: TextStyle(color: Colors.white, fontSize: 16.0),),
            onTap: () {
              // Update the state of the app
            },
          ),
          ListTile(
            title: Text('Edit Profile', style: TextStyle(color: Colors.white, fontSize: 16.0),),
            onTap: () {
              // Update the state of the app
            },
          ),
          ListTile(
            title: Text('Change Login Credentials', style: TextStyle(color: Colors.white, fontSize: 16.0),),
            onTap: () {
              // Update the state of the app
            },
          ),
          ListTile(
            trailing: Icon(Icons.exit_to_app),
            title: Text('Logout', style: TextStyle(color: Colors.white, fontSize: 16.0),),
            onTap: () {
              // Update the state of the app
            },
          ),
        ],
      ),
    );
    
  }

  Widget buildBody(BuildContext context) {
      return CustomScrollView(
        slivers: <Widget>[
          new SliverAppBar(
            pinned: true,
            floating: false, 
            expandedHeight: 450.0,
            flexibleSpace: new FlexibleSpaceBar(
              background: new Center(child: Container(
                    decoration: BoxDecoration(color: TasselColors.dark_blue),
                    child: new Column(children: <Widget>[
                    new SizedBox(height: 50.0,),
                    apiService.getUserImage(),
                    new SizedBox(height: 20.0,),
                    new Text("First Lastname", style: TextStyle(
                      color: Colors.white,
                      fontSize: 20.0
                    ),),
                    _buildMapSection(),
                    _buildButtonRow()
                    ]
                  )
                ),
              )
            ),
          ),
          new SliverGrid(
            gridDelegate: new SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 200.0,
              mainAxisSpacing: 10.0,
              crossAxisSpacing: 10.0,
              childAspectRatio: 4.0,
            ),
            delegate: new SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                return new Container(
                  alignment: Alignment.center,
                  color: Colors.teal[100 * ((index % 9)+1)],
                  child: new Text('xx x $index'),
                );
              },
              childCount: 20,
            ),
          ),
          new SliverFixedExtentList(
            itemExtent: 50.0,
            delegate: new SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                return new Container(
                  alignment: Alignment.center,
                  color: Colors.lightBlue[100 * ((index % 9)+1)],
                  child: new Text('list item $index'),
                );
              },
            ),
          ),
        ],
      );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      endDrawer: buildDrawer(context),      
      body: buildBody(context),);
  }
}