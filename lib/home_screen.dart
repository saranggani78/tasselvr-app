import 'package:flutter/material.dart';
import 'package:parse_fullstack/cities_list.dart';
import 'package:parse_fullstack/private_profile.dart';
import 'package:parse_fullstack/trips_list.dart';

class HomeScreen extends StatefulWidget {
  final String currentUserId;
  HomeScreen({Key key, this.currentUserId}) : super(key: key);
  @override
  HomePageState createState() => new HomePageState(currentUserId);
}

class HomePageState extends State<HomeScreen>{
  final PageController controller = new PageController();
  int _selectedIndex = 0;
  HomePageState(String uid);

  BottomNavigationBar _buildFooter(){ 
    return new BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      currentIndex: _selectedIndex,
      onTap: (int index) {
        setState(() {
          _selectedIndex = index;
          controller.animateToPage(
            _selectedIndex,
            duration: kTabScrollDuration,
            curve: Curves.fastOutSlowIn,
          );
        });
      },
      items: <BottomNavigationBarItem>[
        new BottomNavigationBarItem(
          icon: new Icon(Icons.home),
          title: new Text('Discover', style: TextStyle(color: Colors.white),),
        ),
        new BottomNavigationBarItem(
          icon: new Icon(Icons.add),
          title: new Text('Trips', style: TextStyle(color: Colors.white)),
        ),
        new BottomNavigationBarItem(
          icon: new Icon(Icons.person),
          title: new Text('Profile', style: TextStyle(color: Colors.white)),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context){
    return new Scaffold(
      // backgroundColor: Colors.white,
      body: new PageView(
        controller: controller,
        onPageChanged: (index) => setState(() => _selectedIndex = index),
        children: <Widget>[
          CitiesListScreen(),
          TripListScreen(),
          PrivateProfileScreen(userId: widget.currentUserId,isPrivate: true,),
          // TODO use tripId to reference messages
          // TODO ContactListScreen(currentUserId: widget.currentUserId), // // => TODO ChatListScreen()
          // TODO PublicProfileScreen(title:"Public Profile")(),  // => TODO PrivateProfileScreen()
        ],
      ),
      bottomNavigationBar: _buildFooter(), 
    );
  }
}
