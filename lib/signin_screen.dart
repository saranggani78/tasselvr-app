import 'package:flutter/material.dart';
import 'package:parse_fullstack/login_service.dart';
import 'dart:async';

class SignInScreen extends StatefulWidget {
  @override
  _ParseSignInState createState() => _ParseSignInState();
}

class _ParseSignInState extends State<SignInScreen> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final TextEditingController _pwdController = new TextEditingController();
  final TextEditingController _userController = new TextEditingController();

  final LoginService loginService = new LoginService();
  bool isSignedIn = false;

  void submitForm(BuildContext context) async {
    String username = _userController.text;
    String password = _pwdController.text;

    bool didSignUp = await loginService.handleSignIn(username: username, password: password);
    print("SIGN UP : $didSignUp");
    if(didSignUp){
      Navigator.pushReplacementNamed(context, "/home");
    }

  }

  Future<bool> checkSession() async {
    return await loginService.checkCurrentSession();
  }

  @override
  void initState() {
    checkSession().then((isSignedIn){
      print("SIGNED IN $isSignedIn");
      if(isSignedIn){
        Navigator.pushReplacementNamed(context, "/home");
      }
      setState(() {
        isSignedIn = isSignedIn;
      });
    }).catchError((onError){print("CHECK SESSION ERROR $onError");});
    super.initState();
    }

  //  use TextField.onChanged listener to implement clear text button
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: new Form(
            key: _formKey,
            child: new Column(
              children: <Widget>[
                SizedBox(height: 20.0,),
                Text("Sign In",
                  style: TextStyle(fontSize: 30.0),
                
                ),
                SizedBox(height: 40.0,),
                Stack(
                  alignment: const Alignment(1.0, 1.0),
                  children: <Widget>[
                    TextFormField(
                      controller: _userController,
                      decoration: InputDecoration(
                        labelText: "Username",
                      ),
                      // onSaved: (field) => data.addAll({"fieldkey": field}),
                      keyboardType: TextInputType.text,
                    ),
                    IconButton(icon: Icon(Icons.cancel), onPressed: (){_userController.clear();})
                  ]
                ),
                SizedBox(height: 50.0,),
                Stack(
                  alignment: const Alignment(1.0, 1.0),
                  children: <Widget>[
                  TextFormField(
                    controller: _pwdController,
                    decoration: InputDecoration(
                      labelText: "Password",
                    ),
                    // onSaved: (field) => data.addAll({"fieldkey": field}),
                    keyboardType: TextInputType.text,
                    obscureText: true,
                  ),
                  IconButton(
                      icon: Icon(Icons.cancel),
                        onPressed: (){_pwdController.clear();})
                ],),
                SizedBox(height: 20.0,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                  RaisedButton(
                    onPressed: (){submitForm(context);},
                    // child: Text("Sign in", style: TextStyle(fontSize: 16.0),),
                    child: Icon(Icons.send),
                    padding: EdgeInsets.fromLTRB(30.0, 15.0, 30.0, 15.0),
                    color: Color(0xffdd4b39),
                  )
                ],),
                SizedBox(height: 20.0),
                GestureDetector(
                  child: Text("Sign Up", style: TextStyle(fontSize: 16.0),),
                  onTap: (){
                    Navigator.pushNamed(context, "/signup");
                  },
                ), 
                SizedBox(height: 50.0),
                GestureDetector(
                  child: Text("Reset Password", style: TextStyle(fontSize: 16.0),),
                  onTap: (){
                    Navigator.pushNamed(context, "/reset");
                  },
                ), 
                ],)
            ),
          ),
        )
    );
  }
}

