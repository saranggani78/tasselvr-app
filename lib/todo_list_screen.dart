import 'dart:async';
import 'package:flutter/material.dart';


// TODO pass in user id and todo list id
class TodoList extends StatefulWidget {
  _TodoListState createState() => _TodoListState();
}

class _TodoListState extends State<TodoList> {

  Widget getTitle() {
    return Text("Todo Title");
  }

  // TODO parse sdk query todo items given todo list id
  Stream onTodoListUpdate() {

  }

  Widget getTodoItemList() {
    return StreamBuilder(
      stream: onTodoListUpdate(),
      // initialData: initialData ,
      builder: (BuildContext context, AsyncSnapshot snapshot){
        return ListView.builder(
          itemBuilder: (BuildContext context, int index){return Container();},
        );
      },
    ); 
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: getTitle(),),
      body: getTodoItemList(),
      // bottomSheet: ,
      persistentFooterButtons: <Widget>[
        // TODO text form input 
        // TODO send button
        // TODO send offer button
      ],
    );
  }
}