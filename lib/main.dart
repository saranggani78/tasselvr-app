import 'package:flutter/material.dart';
import 'routes.dart';
import 'tassel_colors.dart';
import 'parse_rest_service.dart';

void main() {
  ParseService.getInstance();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.teal[400],
        canvasColor: TasselColors.dark_green,

      ),
      // home: MyHomePage(title: "User Contacts"),
      routes: Routes(context).routes,
    );
  }
}
