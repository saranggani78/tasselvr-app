import 'package:flutter/material.dart';

// ignore_for_file: non_constant_identifier_names

class TasselColors {
  static Color get dark_teal => Color.fromRGBO(46,97,112,1.0);
  static Color get unknown1 => Color.fromRGBO(26, 54, 64, 1.0);
  static Color get dark_blue => Color.fromRGBO(65,75,110,1.0);
  static Color get dark_green => Color.fromRGBO(35, 45, 75, 1.0);
}
