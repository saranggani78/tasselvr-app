// https://github.com/rohan20/flutter-chat-app/blob/master/lib/ChatScreen.dart
// https://github.com/ctalladen78/flutter-chat-demo/blob/master/lib/chat.dart

import 'package:flutter/material.dart';
import 'package:parse_fullstack/chat_thread.dart';
import 'package:parse_fullstack/parse_rest_service.dart';
import 'tassel_colors.dart';
import 'user_avatar.dart';

var currentUserEmail;

class ChatScreen extends StatefulWidget {
  final String currentUserId;

  ChatScreen({Key key, this.currentUserId}) : super(key: key);

  @override
  ChatScreenState createState() {
    return new ChatScreenState();
  }
}

class ChatScreenState extends State<ChatScreen> {
  ParseService parseService = ParseService.getInstance();

  Widget buildConnectionItem(){
    return ListTile(
      onTap: (){ Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ChatScreen(currentUserId: widget.currentUserId)),
        );
      }

    );
  }

  Widget buildConnectionsList(){
    String defConnectionId = "test123";
    var list = List<String>();
    list.add(defConnectionId);

    return FutureBuilder<List<String>>(
      future: parseService.getConnectionsList(userId: widget.currentUserId), // list of connections
      initialData: list,// default object with required fields
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if(snapshot.hasData && snapshot.connectionState == ConnectionState.done){
          print("CONNECTION ID ${snapshot.data.length}");
          return ListView.builder(
            itemCount: snapshot.data.length,
            itemBuilder: (context, index){
              String connectionUserId = snapshot.data[index];
              print("CONNECTION USRE ID $connectionUserId");
              return GestureDetector(
                onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => ChatThread(chatId: connectionUserId)));
                },
                child:  ListTile(
                  leading: UserAvatar().getAvatar(),
                  title: Text("TEST", style: TextStyle(color: Colors.white, fontSize: 15.0,fontWeight: FontWeight.bold)),
                  subtitle: Text("TEST", style: TextStyle(color: Colors.white, fontSize: 12.0,fontWeight: FontWeight.normal)),
                  trailing: Icon(Icons.more_vert, color: Colors.white,),
                ),
              );
            },
          );
        }
        return Center(child: CircularProgressIndicator(),);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: TasselColors.dark_blue,
        appBar: new AppBar(
          backgroundColor: TasselColors.dark_green,
          title: new Text("Contacts"),
          actions: <Widget>[
            new IconButton(
                icon: new Icon(Icons.exit_to_app), onPressed: (){})
          ],
        ),
        body: new Container(
          child: buildConnectionsList()
        ),
    );
  }
}