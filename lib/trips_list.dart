import 'package:flutter/material.dart';
import 'package:parse_fullstack/parse_rest_service.dart';
import 'package:parse_fullstack/create_trip_form.dart';
import 'tassel_colors.dart';
import 'tassel_ui.dart';

class TripListScreen extends StatefulWidget {
  final String userId;
  TripListScreen({Key key, this.userId}) : super(key: key);

  @override
  MyTabsState createState() => new MyTabsState();
}

class MyTabsState extends State<TripListScreen> with SingleTickerProviderStateMixin  {
  TabController _controller;

  @override
  void initState() {
    super.initState();
    _controller = new TabController(vsync: this, length: 3);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  Widget _buildBody(){
    return new TabBarView(
      controller: _controller,
      children: <Widget>[
        new FirstPage(),
        new SecondPage(),
        new ThirdPage(),
      ]
    );
  }

  @override
  Widget build(BuildContext context){
    return new Scaffold(
      backgroundColor: TasselColors.dark_blue,
      appBar: new AppBar(
        backgroundColor: TasselColors.dark_blue,
        title: new TabBar(
          controller: _controller,
          tabs: <Tab>[
            new Tab(
              text: "Trips",
              // icon: new Icon(Icons.arrow_forward)
            ),
            new Tab(
              text: "Offers",
              // icon: new Icon(Icons.arrow_downward)
            ),
            // new Tab(icon: new Icon(Icons.arrow_back)),
          ]
        )
      ),
      body: _buildBody(),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.tealAccent[700],
        onPressed: (){
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => CreateTripForm(userId: widget.userId,)),
          );
        },
        child: Icon(Icons.add),
      ),
    );
  }
}

class FirstPage extends StatelessWidget {

  Widget buildBodyList(){
    return CustomScrollView(
      scrollDirection: Axis.vertical,
      slivers: <Widget>[
        // https://stackoverflow.com/questions/50528665/how-to-create-a-transparent-ui-in-flutter

        TasselUi().buildUserList()
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
        child: buildBodyList()
    );
  }
}

class SecondPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Center(
        child: new Icon(Icons.favorite,  size: 100.0, color:
        Colors.brown)
      )
    );
  }
}

class ThirdPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Center(
        child: new Icon(Icons.person,  size: 100.0, color:
        Colors.brown)
      )
    );
  }
}














