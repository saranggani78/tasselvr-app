import 'package:flutter/material.dart';
import 'models/user_data.dart';
import 'public_profile.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'parse_rest_service.dart';
import 'user_avatar.dart';

class TasselUi {
  ParseService parseService = ParseService.getInstance();

  Widget buildUserTitle({name: String, city: String, imageUrl: String}){
    return Container(
        decoration: BoxDecoration(
            color: Colors.black54
        ),
        child: ListTile(
          leading: UserAvatar().getAvatar(),
          title: Text(name, style: TextStyle(color: Colors.white, fontSize: 15.0, fontWeight: FontWeight.bold)),
          subtitle: Text(city, style: TextStyle(color: Colors.white, fontSize: 12.0, fontWeight: FontWeight.normal)),
          trailing: Icon(Icons.more_vert, color: Colors.white,),
        )
    );
  }

  // https://docs.flutter.io/flutter/painting/BoxDecoration-class.html
  Widget buildUserItem(context, UserData user){
    // String image = city.image;
    // String cityName = city.city;
    // String country = city.country;
    // String cityId = city.objectId;
    return Padding(
        padding: EdgeInsets.symmetric(horizontal:10.0),
        child: GestureDetector(
            onTap: (){ Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => PublicProfileScreen(userId: user.objectId, isPrivate: false,)),
            );
            },
            child: Card(
                elevation: 2.0,
                child: Container(
                    height: 110.0, // adjust user height
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: CachedNetworkImageProvider(user.locationImage),
                            fit: BoxFit.cover
                        )
                    ),
                    alignment: Alignment.bottomLeft,
                    child: Container(
                        height: 60.0,
                        child: buildUserTitle(name: user.firstname, city: user.city) // get city name
                    )
                )
            )
        )
    );
  }

  Widget buildUserList(){
    return FutureBuilder<List<UserData>>(
        future: parseService.user_list,
        initialData: [UserData(location: "cityObjectId",locationImage: "url",firstname: "test",objectId: "A123",city: "Texas")],
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if(snapshot.hasData && snapshot.connectionState == ConnectionState.done){
            var snapList = snapshot.data;
            return SliverList(
              delegate: SliverChildBuilderDelegate((context,index){
                return Container(
                    margin: EdgeInsets.only(bottom: 20.0),
                    child: buildUserItem(context, snapList[index])
                );
              }, childCount: snapList.length),
            );
          }
          return SliverFillRemaining(child: Center(child: CircularProgressIndicator()));
        }
    );
  }

}