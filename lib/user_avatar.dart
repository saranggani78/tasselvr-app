import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';

class UserAvatar {
  Future<Widget> _getUserImageUri(http.Client client) async {
    var response = await client.get('https://randomuser.me/api/');
    if(response.statusCode == 200){
      print("Image Uri success");
      Map<String, dynamic> parsed = jsonDecode(response.body);
      var uri = parsed["results"][0]["picture"]["large"];
      return new CircleAvatar(
        radius: 22.0,
        backgroundColor: Colors.white,
        child: new CircleAvatar(
            radius: 20.0,
            backgroundImage: new NetworkImage(uri)
        ),
      );
    }
    print("Image Uri error");
    return Center(child: CircularProgressIndicator(),);
  }

  Widget getDefaultAvatar(){
    return CircleAvatar(
        radius: 20.0,
        backgroundColor: Colors.grey,
        child: Icon(Icons.person, size: 40.0)
    );
  }

  Widget getAvatar(){
    return FutureBuilder(
      future: _getUserImageUri(http.Client()),
      initialData: getDefaultAvatar(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if(snapshot.hasData && snapshot.connectionState == ConnectionState.done){

        }
        return snapshot.data;
      },
    );
  }
}