import 'package:flutter/material.dart';
import 'package:parse_fullstack/login_service.dart';
import 'toast_is_snackbar.dart';
import 'dart:async';

class ParseSignUp extends StatefulWidget {
  @override
  _ParseSignUpState createState() => _ParseSignUpState();
}

class _ParseSignUpState extends State<ParseSignUp> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final TextEditingController _emailController = new TextEditingController();
  final TextEditingController _pwdController = new TextEditingController();
  final TextEditingController _userController = new TextEditingController();

  final LoginService loginService = new LoginService();

  Future<bool> submitForm(BuildContext context) async {
    String username = _userController.text;
    String email = _emailController.text;
    String password = _pwdController.text;

    bool didSignUp = await loginService.signUp(username: username, email: email, pwd: password);
    // bool isValidEmail = await loginService.emailValidate(email:email);
    if(didSignUp){
      Fluttertoast.showToast(context, msg:"Nice! Next, please check your inbox");
      Navigator.pop(context);
    }
    else{
      Fluttertoast.showToast(context, msg:"Invalid email");
    }
    return didSignUp;
  }

  @override
  void initState() {
    // TODO: implement initState
    
      super.initState();
    }

  //  use TextField.onChanged listener to implement clear text button
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: new Form(
            key: _formKey,
            child: new Column(
              children: <Widget>[
                SizedBox(height: 20.0,),
                Text("Sign In",
                  style: TextStyle(fontSize: 30.0),
                
                ),
                SizedBox(height: 40.0,),
                Stack(
                  alignment: const Alignment(1.0, 1.0),
                  children: <Widget>[
                    TextFormField(
                      controller: _userController,
                      decoration: InputDecoration(
                        labelText: "Username",
                      ),
                      // onSaved: (field) => data.addAll({"fieldkey": field}),
                      keyboardType: TextInputType.text,
                    ),
                    IconButton(
                      icon: Icon(Icons.cancel),
                          onPressed: (){_emailController.clear();})
                  ]
                ),
                SizedBox(height: 50.0,),
                Stack(
                  alignment: const Alignment(1.0, 1.0),
                  children: <Widget>[
                  TextFormField(
                    controller: _pwdController,
                    decoration: InputDecoration(
                      labelText: "Password",
                    ),
                    // onSaved: (field) => data.addAll({"fieldkey": field}),
                    keyboardType: TextInputType.text,
                    obscureText: true,
                  ),
                  IconButton(
                      icon: Icon(Icons.cancel),
                        onPressed: (){_emailController.clear();})
                ],),
                SizedBox(height: 50.0,),
                Stack(
                  alignment: const Alignment(1.0, 1.0),
                  children: <Widget>[
                    TextFormField(
                      controller: _emailController,
                      decoration: InputDecoration(
                        labelText: "Email",
                      ),
                      // onSaved: (field) => data.addAll({"fieldkey": field}),
                      keyboardType: TextInputType.emailAddress,
                    ),
                    IconButton(
                      icon: Icon(Icons.cancel),
                          onPressed: (){_emailController.clear();})
                  ]
                ),
                SizedBox(height: 50.0,),

                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  
                  children: <Widget>[
                  FloatingActionButton(
                    backgroundColor: Colors.black,
                    child: Icon(Icons.cancel,
                      size: 56.0, 
                      color: Colors.grey,
                    ),
                    onPressed: (){Navigator.pop(context);},
                  ),
                  SizedBox(width: 30.0,),
                  RaisedButton(
                    onPressed: () async {
                      await submitForm(context);
                      },
                    child: Text("Sign up"),
                    padding: EdgeInsets.fromLTRB(30.0, 15.0, 30.0, 15.0),
                    color: Color(0xffdd4b39),
                  )
                ],)
                  // RaisedButton(
                  //   onPressed: (){submitForm(context);},
                  //   child: Text("Sign up"),
                  //   padding: EdgeInsets.fromLTRB(30.0, 15.0, 30.0, 15.0),
                  //   color: Color(0xffdd4b39),
                  // )
                ],)
            ),
          ),
        )
    );
  }
}

