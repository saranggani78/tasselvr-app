import 'package:flutter/material.dart';
import 'package:parse_fullstack/parse_rest_service.dart';

class CreateOffer extends StatefulWidget {
  _PageState createState() => _PageState();
}

// TODO on submit form create a new trip object
class _PageState extends State<CreateOffer> {

  void _cancelTrip() {
    // TODO Navigator.pop  // back to home

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("New Trip"),
        actions: <Widget>[
          GestureDetector(
            child: Text("Cancel"),
            onTap: _cancelTrip,
          )
        ]
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[

          ],
        ),
      ),
    );
  }
}