import 'package:flutter/material.dart';

class MyDatePicker extends StatelessWidget {
  MyDatePicker({Key key, this.title}) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(title),
      ),
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            // new ExtendedTextField(
            //   hintText: 'Center aligned hint text :D',
            //   textAlign: TextAlign.center,
            // ),
            // new DatePicker(),
            // new TimePicker(),
          ],
        ),
      ),
    );
  }
}
