import 'package:flutter/material.dart';
import 'package:parse_fullstack/login_service.dart';
import 'toast_is_snackbar.dart';

class ResetPasswordForm extends StatefulWidget {
  @override
  _ParseSignUpState createState() => _ParseSignUpState();
}

class _ParseSignUpState extends State<ResetPasswordForm> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final TextEditingController _userController = new TextEditingController();

  final LoginService loginService = new LoginService();

  void submitForm(BuildContext context) async {
    String email = _userController.text;

    var didReset = await loginService.resetPassword(email: email);
    print("RESET $didReset");
    if(didReset){
      Fluttertoast.showToast(context, msg:"Ok, please check your inbox");
      Navigator.pop(context); 
    }
    else{
      Fluttertoast.showToast(context, msg:"Invalid email");
    }
    
  }

  @override
  void initState() {
    // TODO: implement initState
    
      super.initState();
    }

  //  use TextField.onChanged listener to implement clear text button
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: new Form(
            key: _formKey,
            child: new Column(
              children: <Widget>[
                SizedBox(height: 20.0,),
                Text("Reset Password",
                  style: TextStyle(fontSize: 30.0),
                
                ),
                SizedBox(height: 40.0,),
                Stack(
                  alignment: const Alignment(1.0, 1.0),
                  children: <Widget>[
                    TextFormField(
                      controller: _userController,
                      decoration: InputDecoration(
                        labelText: "Email",
                      ),
                      // onSaved: (field) => data.addAll({"fieldkey": field}),
                      keyboardType: TextInputType.text,
                    ),
                    IconButton(
                      icon: Icon(Icons.cancel),
                          onPressed: (){_userController.clear();})
                  ]
                ),
                SizedBox(height: 50.0,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                  FloatingActionButton(
                    backgroundColor: Colors.black,
                    child: Icon(Icons.cancel,
                      size: 56.0, 
                      color: Colors.grey,
                    ),
                    onPressed: (){Navigator.pop(context);},
                  ),
                  SizedBox(width: 30.0,),
                  RaisedButton(
                    onPressed: () async {
                      submitForm(context);
                    },
                    child: Text("Reset password"),
                    padding: EdgeInsets.fromLTRB(30.0, 15.0, 30.0, 15.0),
                    color: Color(0xffdd4b39),
                  )
                ],)
                  // RaisedButton(
                  //   onPressed: (){submitForm(context);},
                  //   child: Text("Sign up"),
                  //   padding: EdgeInsets.fromLTRB(30.0, 15.0, 30.0, 15.0),
                  //   color: Color(0xffdd4b39),
                  // )
                ],)
            ),
          ),
        )
    );
  }
}

