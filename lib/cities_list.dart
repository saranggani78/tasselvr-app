import 'package:flutter/material.dart';
import 'dart:math' as math;
import 'package:parse_fullstack/login_service.dart';
import 'package:parse_fullstack/parse_rest_service.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:parse_fullstack/user_list.dart';
import 'package:parse_fullstack/models/city.dart';
import 'package:parse_fullstack/chat_list.dart';
import 'tassel_colors.dart';
import 'tassel_ui.dart';

// https://codelabs.developers.google.com/codelabs/flutter-firebase/
class CitiesListScreen extends StatefulWidget {
  final String currentUserId ;
  CitiesListScreen({Key key, this.currentUserId}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<CitiesListScreen> {

  LoginService loginService = new LoginService();

  final TextEditingController _nameController = new TextEditingController();
  ParseService parseService = ParseService.getInstance();

  _MyHomePageState();

  void _logout() async {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ChatScreen(currentUserId: widget.currentUserId)),
    );
  }

  @override
  void initState() {
    // TODO user list cache 
    // TODO get app state from store
    super.initState();
  }
  
  @override
  void dispose() {
      _nameController.dispose();
      super.dispose();
    }
  
  Widget buildHeaderTitle(String title){
    return Container(
      margin: EdgeInsets.only(left: 20.0, top: 20.0,bottom: 20.0),
      child: Text(title, textAlign: TextAlign.start, style: TextStyle(color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.bold))
    );
  }

  Widget buildCityTitle({city: String, country: String}){
    return Container(
      height: 40.0,  // adjust title height
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: Colors.black54,
        borderRadius: BorderRadius.vertical(bottom: Radius.circular(5.0))
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(height: 10.0,),
          Text(city, textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontSize: 12.0,fontWeight: FontWeight.bold)),
          // SizedBox(height: 5.0,),
          // Text(country, textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontSize: 10.0,fontWeight: FontWeight.normal)),
      ])
    );
  }


  Widget buildCityItem(City city){
    String image = city.image;
    String cityName = city.city;
    String country = city.country;
    String cityId = city.objectId;

    return GestureDetector(
      
      onTap: (){ Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => UserListScreen(cityId: cityId,city: cityName,)),
          );
        },
      child: Card(
            margin: EdgeInsets.symmetric(horizontal: 5.0),
            child: Container(
              height: 190.0, // adjust height
              width: 100.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5.0)),
                image: DecorationImage(
                  image: CachedNetworkImageProvider(image),
                  fit: BoxFit.cover
                )
              ),
              alignment: Alignment.bottomLeft,
              child: buildCityTitle(city: cityName, country: country)
            ),
        )
    );
  }
  
  Widget buildHorizontalList() {
    return SliverPersistentHeader(
                  delegate: _SliverAppBarDelegate(
                  minHeight: 230.0,
                  maxHeight: 230.0,
                  child: FutureBuilder(
                    future: parseService.city_list,
                    // initialData: InitialData,
                    builder: (BuildContext context, AsyncSnapshot snapshot) {
                       if(snapshot.hasData && snapshot.connectionState == ConnectionState.done){
                         var list = snapshot.data;
                         return Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              buildHeaderTitle("Top Cities"),
                              Container(
                                height: 160.0, // adjust section height
                                child: ListView.builder( itemCount: list.length,
                                  scrollDirection: Axis.horizontal,
                                  itemBuilder: (context, idx){
                                    return buildCityItem(list[idx]);
                                  },
                                ),
                              ),
                            ],
                          );
                        }
                        return Center(child:CircularProgressIndicator());
                    },
                  ),
                )
    );
  }

  Widget buildBodyList(){
    return CustomScrollView(
            scrollDirection: Axis.vertical,
            slivers: <Widget>[
              buildHorizontalList(),
              SliverAppBar(
                automaticallyImplyLeading: false,            
                centerTitle: false,
                title: Text("Top Locals & Travelers", textAlign: TextAlign.start, style: TextStyle(color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.bold)),
                backgroundColor: Colors.transparent,
              ),
              TasselUi().buildUserList()
            ],
          );
        }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: TasselColors.dark_blue,
        appBar: new AppBar(
          backgroundColor: TasselColors.dark_green,
            title: Text('Tassel App', style: TextStyle(color: Colors.tealAccent[700],)),
            actions: <Widget>[ 
              IconButton(
                padding: EdgeInsets.symmetric(horizontal: 15.0),
                icon: Icon(Icons.chat, size: 25.0,),
                onPressed: (){ 
                  _logout();
                },
              )
            ],
          ),
      body: buildBodyList(),
      
    );
  }
}

// https://medium.com/flutter-io/slivers-demystified-6ff68ab0296f
class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate({
    @required this.minHeight,
    @required this.maxHeight,
    @required this.child,
  });
  final double minHeight;
  final double maxHeight;
  final Widget child;
  @override
  double get minExtent => minHeight;
  @override
  double get maxExtent => math.max(maxHeight, minHeight);
  @override
  Widget build(
      BuildContext context, 
      double shrinkOffset, 
      bool overlapsContent) 
  {
    return new SizedBox.expand(child: child);
  }
  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return maxHeight != oldDelegate.maxHeight ||
        minHeight != oldDelegate.minHeight ||
        child != oldDelegate.child;
  }
}