class Review{  
      String rating;
      String remarks;
      String tripReference;
      String createdBy; 
    
      Review({ 
        this.rating,this.remarks,this.tripReference,this.createdBy,
      });
      
      static Review fromJson(Map<String,dynamic> json){
        return Review( 
            rating: json['rating'],
            remarks: json['remarks'],
            tripReference: json['tripReference'],
            createdBy: json['createdBy'],
        );
      }
      
      Map<String, dynamic> toJson() => { 
            'rating': rating,
            'remarks': remarks,
            'tripReference': tripReference,
            'createdBy': createdBy,
      };
    }