class ChatMessage{  
      String createdBy;
      String userTo;
      String chatThreadReference;
      String text; 
    
      ChatMessage({ 
        this.createdBy,this.userTo,this.chatThreadReference,this.text,
      });
      
      static ChatMessage fromJson(Map<String,dynamic> json){
        return ChatMessage( 
            createdBy: json['createdBy'],
            userTo: json['userTo'],
            chatThreadReference: json['chatThreadReference'],
            text: json['text'],
        );
      }
      
      Map<String, dynamic> toJson() => { 
            'createdBy': createdBy,
            'userTo': userTo,
            'chatThreadReference': chatThreadReference,
            'text': text,
      };
    }