class ChatThread{  
      String createdBy;
      String userTo;
      String tripReference; 
    
      ChatThread({ 
        this.createdBy,this.userTo,this.tripReference,
      });
      
      static ChatThread fromJson(Map<String,dynamic> json){
        return ChatThread( 
            createdBy: json['createdBy'],
            userTo: json['userTo'],
            tripReference: json['tripReference'],
        );
      }
      
      Map<String, dynamic> toJson() => { 
            'createdBy': createdBy,
            'userTo': userTo,
            'tripReference': tripReference,
      };
    }