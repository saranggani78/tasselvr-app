class Offer{  
      String createdBy;
      String userTo;
      String tripReference;
      String paymentReference;
      String expirationDate; 
    
      Offer({ 
        this.createdBy,this.userTo,this.tripReference,this.paymentReference,this.expirationDate,
      });
      
      static Offer fromJson(Map<String,dynamic> json){
        return Offer( 
            createdBy: json['createdBy'],
            userTo: json['userTo'],
            tripReference: json['tripReference'],
            paymentReference: json['paymentReference'],
            expirationDate: json['expirationDate'],
        );
      }
      
      Map<String, dynamic> toJson() => { 
            'createdBy': createdBy,
            'userTo': userTo,
            'tripReference': tripReference,
            'paymentReference': paymentReference,
            'expirationDate': expirationDate,
      };
    }