class Invitation{  
      String userFrom;
      String userTo;
      String tripReference; 
    
      Invitation({ 
        this.userFrom,this.userTo,this.tripReference,
      });
      
      static Invitation fromJson(Map<String,dynamic> json){
        return Invitation( 
            userFrom: json['userFrom'],
            userTo: json['userTo'],
            tripReference: json['tripReference'],
        );
      }
      
      Map<String, dynamic> toJson() => { 
            'userFrom': userFrom,
            'userTo': userTo,
            'tripReference': tripReference,
      };
    }