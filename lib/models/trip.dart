class Trip{  
      String createdBy;
      String guide;
      List<String> inviteeList;
      String city;
      String description;
      String startDate;
      String isReviewed;
      String duration;
      String paymentReference; 
    
      Trip({ 
        this.description,this.createdBy,this.guide,this.inviteeList,this.startDate,this.isReviewed,this.duration,this.paymentReference,this.city
      });
      
      // https://medium.com/flutter-community/parsing-complex-json-in-flutter-747c46655f51
      static Trip fromJson(Map<String,dynamic> json){

        var inviteesFromJson  = json['inviteeList'];
        List<String> invitees = inviteesFromJson.cast<String>();

        return Trip( 
            description: json['description'],
            createdBy: json['createdBy'],
            city: json['city'],
            guide: json['guide'],
            inviteeList: invitees,
            startDate: json['startDate'],
            isReviewed: json['isReviewed'],
            duration: json['duration'],
            paymentReference: json['paymentReference'],
        );
      }
      
      Map<String, dynamic> toJson() => { 
            'description': description,
            'createdBy': createdBy,
            'city': city,
            'guide': guide,
            'inviteeList': inviteeList,
            'startDate': startDate,
            'isReviewed': isReviewed,
            'duration': duration,
            'paymentReference': paymentReference,
      };
    }