
/* response from login 
{
   "objectId":"vUmycHPawh",
   "email":"er@sds.co",
   "username":"texas",
   "createdAt":"2019-01-13T08:30:25.853Z", // date format : yyyy-MM-dd'T'HH:mm:ssZ 
   "updatedAt":"2019-01-13T14:12:29.227Z",
   "ACL":{
      "*":{
         "read":true
      },
      "vUmycHPawh":{
         "read":true,
         "write":true
      }
   },
   "sessionToken":"r:202bce467b6eb3e93bdbc410ff753cf4"
}
*/
class UserData{  
      String objectId;
      String email;
      String username;
      String createdAt;
      String updatedAt;
      String sessionToken; 
      String firstname;
      String lastname;
      String motto;
      String image;
      bool isLocalHost;
      String location;
      String locationImage;
      String city;


      UserData({
        this.objectId,
        this.email,
        this.username,
        this.createdAt,
        this.updatedAt,
        this.sessionToken,
        this.firstname,
        this.lastname,
        this.motto,
        this.image,
        this.isLocalHost,
        this.location,
        String locationImage,
        String city 
      });

      static String default_to_empty(json_value) {
        return json_value == null ? ""  : json_value;
      }

      static UserData fromJson(Map<String,dynamic> json){
        return UserData( 
            image: json['image'],
            objectId: json['objectId'],
            email: json['email'],
            username: json['username'],
            firstname: default_to_empty(json['firstname']),
            lastname: default_to_empty(json['lastname']),
            motto: json['motto'],
            location: json['currentLocation']['objectId'],
            createdAt: json['createdAt'],
            updatedAt: json['updatedAt'],
            sessionToken: json['sessionToken'],
        );
      }
      
      Map<String, dynamic> toJson() => { 
            'objectId': objectId,
            'email': email,
            'username': username,
            'createdAt': createdAt,
            'updatedAt': updatedAt,
            'sessionToken': sessionToken,
      };
    }
    // http://json2dart.com/
