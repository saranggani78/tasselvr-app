class City{  
      String city;
      String country;
      String image; 
      String objectId;
    
      City({ 
        this.objectId, this.city,this.country,this.image,
      });
      
      static City fromJson(Map<String,dynamic> json){
        return City( 
            city: json['city'],
            country: json['country'],
            image: json['image'],
            objectId: json['objectId'],
        );
      }
      
      Map<String, dynamic> toJson() => { 
            'city': city,
            'country': country,
            'image': image,
      };
    }