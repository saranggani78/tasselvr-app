import 'dart:async';
import 'package:flutter/material.dart';
import 'package:latlong/latlong.dart'; // get current location and convert to nearest city
import 'package:flutter_map/flutter_map.dart';

// ignore_for_file: non_constant_identifier_names

class ApiService {

  final String GOOGLE_PLACES_TOKEN = ""; // used in places search and city search
  final String MAPBOX_TOKEN = ""; // used in places search and city search

  /**
   * precondition : prefs.getString("isFirstInstall") == true
   */
  // get cities local from firebase storage
  void downloadCities() async {
    // TODO create assets localfile directory
  }

  // https://pub.dartlang.org/packages/google_maps_webservice#-readme-tab-
  Future<Place> getPlacesAutocomplete() async {
    // TODO get google places api token to search places

  }

  /**
   * TODO save api keys, url template string in remote database
   */
  Widget buildStaticMap({ latitude : String, longitude :String, width : int, height : int}) {
    // budapest lat : 47.497913 long : 19.040236
    double lat = 47.497913;
    double long = 19.040236;
    if(latitude == null || longitude == null){latitude = lat; longitude = long;}
    String url = _buildMapBoxUrlString(lat: latitude.toString(), long: latitude.toString(), width: 600, height: 500);
    return new FlutterMap(
      options: new MapOptions(
        center: new LatLng(lat, long),
        zoom: 14.0,
        interactive: false,
      ),
      layers: [
        new TileLayerOptions(
          urlTemplate: url,
          // tileSize: 350,
          // additionalOptions: {
          //   'accessToken': '<PUT_ACCESS_TOKEN_HERE>',
          //   'id': 'mapbox.streets',
          // },
        ),
        // new MarkerLayerOptions(
        //   markers: [
        //     new Marker(
        //       width: 80.0,
        //       height: 80.0,
        //       point: new LatLng(lat, long),
        //       builder: (ctx) =>
        //       new Container(
        //         child: new FlutterLogo(),
        //       ),
        //     ),
        //   ],
        // ),
      ],
    );
  }

  // https://github.com/apptreesoftware/flutter_map/blob/master/example/lib/pages/map_controller.dart
  // https://www.mapbox.com/help/how-static-maps-work/
  // https://www.mapbox.com/api-documentation/?language=cURL
  String _buildMapBoxUrlString({lat : String, long : String, width: int, height: int}){
    // a+2196f3 : marker shows as letter "A" with color blue in hex
    // ignore: unnecessary_brace_in_string_interps
    String retVal = "https://api.mapbox.com/styles/v1/mapbox/streets-v10/static/pin-l-z+ed47ed(${long},${lat})/${long},${lat},14.25,0,50/${width}x${height}@2x?access_token=${MAPBOX_TOKEN}";
    return retVal;
  }

}

class Place {
  String name;
  String latitude;
  String longitude;
  Place({name : String, latitude : String, longitude : String});

}