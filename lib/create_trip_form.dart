import 'package:flutter/material.dart';
import 'package:parse_fullstack/parse_rest_service.dart';
import 'tassel_colors.dart';

class CreateTripForm extends StatefulWidget {
  final String userId;

  CreateTripForm({Key key, this.userId}) : super(key:key);

  @override
  _CreateTripForm createState(){
    return _CreateTripForm();
  }
}

class _CreateTripForm extends State<CreateTripForm> {
  TextEditingController descriptionCtrl = TextEditingController();
  TextEditingController dateCtrl = TextEditingController();
  TextEditingController durationCtrl = TextEditingController();
  TextEditingController budgetCtrl = TextEditingController();
  // https://v1-dartlang-org.firebaseapp.com/resources/dart-tips/dart-tips-ep-5
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  var inviteeSet = Set<String>(); // unordered & unique

  void _cancelTrip(BuildContext context) {
    Navigator.pop(context);
  }


  void _submitNewTrip() {
    // parseService.createNewTrip(TripObject)

  }

  void selectCity() {

  }

  void showDatePicker() {

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: TasselColors.dark_blue,
        appBar: new AppBar(
            backgroundColor: TasselColors.dark_green,
        title: Text("New Trip"),
        actions: <Widget>[
          GestureDetector(
            child: Text("Cancel"),
            onTap: (){}),
          
        ]
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: new Column(
            children: <Widget>[
              SizedBox(height: 20.0,),
              Text("Sign In",
                style: TextStyle(fontSize: 30.0),
              ),
              Stack(
                  alignment: const Alignment(1.0, 1.0),
                  children: <Widget>[
                    TextFormField(
                      controller: descriptionCtrl,
                      decoration: InputDecoration(
                        labelText: "Username",
                      ),
                      // onSaved: (field) => data.addAll({"fieldkey": field}),
                      keyboardType: TextInputType.text,
                    ),
                    IconButton(icon: Icon(Icons.cancel), onPressed: (){descriptionCtrl.clear();}),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        RaisedButton(
                          onPressed: (){_submitNewTrip();},
                          // child: Text("Sign in", style: TextStyle(fontSize: 16.0),),
                          child: Icon(Icons.send),
                          padding: EdgeInsets.fromLTRB(30.0, 15.0, 30.0, 15.0),
                          color: Color(0xffdd4b39),
                        )
                      ],
                    ),
                  ]
                ),
            ]
                
        ),
      ),
    )
    );
  }
}