import 'package:flutter/material.dart';
import 'home_screen.dart';
import 'signup_screen.dart';
import 'create_trip_form.dart';
import 'reset_password.dart';

class Routes {
  var _routes;
  Map<String, WidgetBuilder> get routes => _routes;
  Routes(BuildContext context) {
    _routes = <String, WidgetBuilder>{
      // "/" : (context) => SignInScreen(),
      "/" : (context) => HomeScreen(currentUserId: "test123",),
      "/home" : (context) => HomeScreen(),
      "/signup" : (context) => ParseSignUp(),
      "/create_trip" : (context) => CreateTripForm(),
      "/reset" : (context) => ResetPasswordForm(),
      // "/contacts" : (context) => ContactListScreen(),
      // "/login_email" : (context) => LoginEmailScreen(),
    };
  }
}