import 'package:flutter/material.dart';
import 'package:parse_fullstack/login_service.dart';
import 'package:parse_fullstack/parse_rest_service.dart';
import 'tassel_colors.dart';
import 'tassel_ui.dart';

// https://codelabs.developers.google.com/codelabs/flutter-firebase/
class UserListScreen extends StatefulWidget {
  final String cityId ;
  final String city;
  UserListScreen({Key key, this.cityId, this.city}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<UserListScreen> {

  LoginService loginService = new LoginService();

  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final TextEditingController _nameController = new TextEditingController();

  _MyHomePageState();

  
  @override
  void initState() {
    super.initState();
  }
  
  @override
  void dispose() {
      _nameController.dispose();
      super.dispose();
    }
  
  Widget buildHeaderTitle(String title){
    return Container(
      margin: EdgeInsets.only(left: 20.0, top: 20.0,bottom: 20.0),
      child: Text(title, textAlign: TextAlign.start, style: TextStyle(color: Colors.tealAccent, fontSize: 20.0, fontWeight: FontWeight.bold))
    );
  }

  Widget buildBodyList(){
    return CustomScrollView(
      scrollDirection: Axis.vertical,
      slivers: <Widget>[
        TasselUi().buildUserList()
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: TasselColors.dark_blue,
      appBar: new AppBar(
        backgroundColor: TasselColors.dark_green,
            title: Text(widget.city, style: TextStyle(color: Colors.tealAccent[700],)),
          ),
      body: buildBodyList(),
    );
  }
}
